<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Megaw &#8211; Multi Purpose HTML Template</title>
<link rel='stylesheet' id='layerslider-css'  href='css/layerslider.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='ls-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='plugins/revslider/public/assets/css/settings.css?ver=5.0.9' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css'  href='plugins/woocommerce/assets/css/woocommerce-layout.css?ver=2.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=2.5.2' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='plugins/woocommerce/assets/css/woocommerce.css?ver=2.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='megamenu-css'  href='maxmegamenu/style.css?ver=762094' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-quick-view-css'  href='plugins/yith-woocommerce-quick-view/assets/css/yith-quick-view.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='plugins/woocommerce/assets/css/prettyPhoto.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css'  href='plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-main-css'  href='plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-font-awesome-css'  href='plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimp-for-wp-checkbox-css'  href='plugins/mailchimp-for-wp/assets/css/checkbox.min.css?ver=2.3.7' type='text/css' media='all' />
<link rel='stylesheet' id='wope-gfont-Montserrat-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-gfont-Open-Sans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-default-style-css'  href='style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-responsive-css'  href='responsive.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='flexslider-style-css'  href='js/flex-slider/flexslider.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-style-css'  href='js/magnific-popup/magnific-popup.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-font-awesome-css'  href='font-awesome/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-font-pe-icon-7-stroke-css'  href='pe-icon-7-stroke/css/pe-icon-7-stroke.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimp-for-wp-form-css'  href='plugins/mailchimp-for-wp/assets/css/form.min.css?ver=2.3.7' type='text/css' media='all' />
<script src='plugins/LayerSlider/static/js/greensock.js?ver=1.11.8'></script>
<script src='js/jquery/jquery.js?ver=3.0.0'></script>
<script src='js/jquery/jquery-migrate.min.js?ver=3.0.1'></script>
<script src='plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js?ver=5.3.2'></script>
<script src='plugins/LayerSlider/static/js/layerslider.transitions.js?ver=5.3.2'></script>
<script src='plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.0.9'></script>
<script src='plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.0.9'></script>
<script src='js/hoverIntent.min.js?ver=1.8.1'></script>
<script src='plugins/megamenu/js/maxmegamenu.js?ver=1.8.2'></script>
<link rel='stylesheet' href='css/custom.css' type='text/css' media='all' />
</head>
<body class="page page-template-default">
<div id="background" style="">
	<div id="page" >
		<div id="header" class="header-text-white header-transparent heading-align-center " >
			<div class='header_bg'></div>
			<div class="header_content">
				<div id="top-bar">
					<div class="wrap">
						<div class="left-top-bar">
							<div class="contact-detail-line">
								<ul>
									<li><i class="fa fa-phone"></i> +91–022–2649 5808 </li>
									<li><i class="fa fa-envelope-o"></i> admission@tsec.edu </li>
									<li><i class="fa fa-clock-o"></i> 10AM - 5PM </li>
								</ul>
							</div>
						</div>
						<div class="right-top-bar">
							<div class="header-social"> <a target="_blank" href=" feed/ "><i class="fa fa-rss"></i></a> <a target="_blank"  href="#"><i class="fa fa-behance"></i></a> <a target="_blank"  href="#"><i class="fa fa-dribbble"></i></a> <a target="_blank"  href="#"><i class="fa fa-facebook"></i></a>
								<div class="cleared"></div>
							</div>
						</div>
						<div class="cleared"></div>
					</div>
				</div>
				<div id="top-bar-open">
					<div class="wrap">
						<div id="topbar-open"><i class="fa fa-angle-down"></i></div>
					</div>
				</div>
				<div class="wrap">
					<div class="left-header">
						<div class="site-logo">
							<h1> <a class="logo-image" href=""> <img class="logo-normal" alt="Megaw" src="images/tseclogo.png" /> <img class="logo-retina"   alt="Megaw" src="images/tseclogo.png" /> </a> </h1>
						</div>
					</div>
		
					<div class="right-header">
						<div class="main-menu">
							<div id="mega-menu-wrap-main-menu" class="mega-menu-wrap">
								<div class="mega-menu-toggle"></div>
								<ul id="mega-menu-main-menu" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="disabled" data-second-click="close" data-breakpoint="600">
									<li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom  mega-menu-item-has-children mega-menu-item-18 mega-align-bottom-left mega-menu-flyout'><a href="adminhome.php">Home</a>
									</li>
									<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-has-children mega-menu-item-1360 mega-align-bottom-left mega-menu-flyout'><a href="index.php">Logout</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- End Main Menu --> 
					</div>
					<div class="cleared"></div>
					<div id="toggle-menu-button"><i class="fa fa-align-justify"></i></div>
					<div class="toggle-menu">
						<div class="menu-toggle-menu-container">
							<ul id="menu-toggle-menu" class="menu">
								<li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom  mega-menu-item-has-children mega-menu-item-18 mega-align-bottom-left mega-menu-flyout'><a href="adminhome.php">Home</a>
									</li>
									<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-has-children mega-menu-item-1360 mega-align-bottom-left mega-menu-flyout'><a href="index.php">Logout</a>
									</li>
							</ul>
						</div>
					</div>
				</div>
					
				<!-- End Header Wrap -->  
				<!-- End Header --> 
				
				<!--Header Content for this page-->
				
				
			</div>
			<!-- End Header Content --> 
		</div>
		<!-- end Header -->
		<div class="cleared"></div>
		<!-- BEGIN CONTENT -->
		<div id="body">
			<div id="content-section1" class="content-section black-text white-bg top-spacing-medium bottom-spacing-medium title-spacing-medium align-center " style="">
				<div class="wrap-column">
					<div class="column1">
						<div class="wrap">
							<div class="content-section-heading">
								<h1 class="content-section-title"> Settings </h1>
							</div>
						</div>
					</div>
					<div class="cleared"></div>
				</div>
				<div class="wrap-column">
					<form method="post" action="meritlist.php">
					<div class="content-column1">
						<div class="widget-entry">
							<div class="icon-box">
								<div class="widget-column2-3  icon-box-horizontal  widget-element-bottom">
									
										
										<h1 style="text-transform: uppercase;"> Select columns to display  </h1>
										<ul class="sortable" style="overflow-y: scroll;height: 300px;border:solid 2px #000;">
													<?php   
													 //load_data_select.php  
													 $link=mysqli_connect("localhost","root","","cap");
														
														$result = mysqli_query($link,"SELECT * from stud_details");
														
														for($i = 0; $i < mysqli_num_fields($result); $i++) {
    														$field_info = mysqli_fetch_field($result);
    														echo'<li><input type="checkbox" name="'.$i.'" id="list"><label for="list">'.$field_info->name.'</label></li>';
														  }
														
												       	?>
												       </ul>
											<div class="cleared"></div>
											
								</div>
								<div class="widget-column1-3  icon-box-horizontal  widget-element-bottom" style="text-align: center;margin-top: 10%">
									<input type="submit" class="button" name="search" value="Update" style="font-weight: bold;margin-top: 10px;padding: 20px 40px;margin-bottom: 50px;" /><br>
									<a href="adminhome.html" class="button" name="search" value="Back" style="font-weight: bold;margin-top: 10px;padding: 20px 47px;">Back</a>
								</div>
							</div>

							<!-- end icon box -->
							
						</div>
						<!-- end widget entry --> </div>
					<div class="cleared"></div>
					
					</form>
				</div>

				<!-- end wrap-column no-wrap --> 
				
			</div>
			<!-- end content section -->
			
			
		</div>
		<div id="footer">
			<div class="wrap-column">
				<div id="footer-widget-container">
					<div class="footer-column">
						<div id="text-2" class="footer-widget widget_text content">
							<div class="footer-widget-title">ABOUT US</div>
							<div class="textwidget">Megaw is elegant business wordpress theme built for many purpose. You can use this premium wordpress themes for any kind of website.
								<div class="footer-social"> <a target="_blank" href="#"><i class="fa fa-dribbble"></i></a> <a target="_blank" href="#"><i class="fa fa-facebook"></i></a> <a target="_blank" href="#"><i class="fa fa-instagram"></i></a> <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a> <a target="_blank" href="#"><i class="fa fa-twitter"></i></a> </div>
							</div>
						</div>
					</div>
					<div class="footer-column">
						<div id="wope_multi_line_menu_widget-2" class="footer-widget widget_wope_multi_line_menu_widget content">
							<div class="footer-widget-title">Quick Links</div>
							<div class='multi_line_menu_container_2'>
								<div class="menu-footer-widget-menu-1-container">
									<ul id="menu-footer-widget-menu-1" class="menu">
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3"><a href="#">Home</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4"><a href="#">About Us</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5"><a href="#">The Team</a></li>
										<li  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6"><a href="#">Clients</a></li>
									</ul>
								</div>
								<div class="menu-footer-widget-menu-2-container">
									<ul id="menu-footer-widget-menu-2" class="menu">
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7"><a href="#">Support Center</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8"><a href="#">Megaw Forum</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9"><a href="#">Ticket System</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10"><a href="#">Contact Us</a></li>
									</ul>
								</div>
								<div class="cleared"></div>
							</div>
						</div>
					</div>
					<div class="footer-column">
						<div id="text-3" class="footer-widget widget_text content">
							<div class="footer-widget-title">Contact Us</div>
							<div class="textwidget">Office: 12345 Little Lonsdale St, Melbourne<br>
								Phone: (123) 123-456<br>
								Fax: (123) 123-456<br>
								E-Mail: johndoe@site.com<br>
								Web: http://site.com</div>
						</div>
					</div>
					<div class="cleared"></div>
				</div>
				<!-- End Footer Widget Container--> 
			</div>
		</div>
		<!-- End Footer -->
			<!-- footer start--><?php include 'footer.php' ?><!-- footer end-->
	</div>
		<!-- End Footer Bottom --> 
	</div>
	<!-- End Page --> 
</div>
<!-- End Site Background -->

<div id="yith-quick-view-modal">
	<div class="yith-quick-view-overlay"></div>
	<div class="yith-wcqv-wrapper">
		<div class="yith-wcqv-main">
			<div class="yith-wcqv-head"> <a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a> </div>
			<div id="yith-quick-view-content" class="woocommerce single-product"></div>
		</div>
	</div>
</div>
<script src='plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=2.5.2'></script> 
<script src='plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script> 
<script src='plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=2.5.2'></script> 
<script src='plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1'></script> 
<script src='plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=2.5.2'></script> 
<script src='plugins/yith-woocommerce-quick-view/assets/js/frontend.js?ver=1.0'></script> 
<script src='plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.5'></script> 
<script src='plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.init.min.js?ver=2.5.2'></script> 
<script src='plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=4.4.2'></script> 
<script src='plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js?ver=2.0'></script> 
<script src='js/jquery/ui/core.min.js?ver=1.11.4'></script> 
<script src='js/jquery/ui/widget.min.js?ver=1.11.4'></script> 
<script src='js/jquery/ui/accordion.min.js?ver=1.11.4'></script> 
<script src='js/script.js?ver=4.4.2'></script> 
<script src='js/jquery.easing.1.3.js?ver=4.4.2'></script> 
<script src='js/jquery.isotope.min.js?ver=4.4.2'></script> 
<script src='js/flex-slider/jquery.flexslider-min.js?ver=4.4.2'></script> 
<script src='js/magnific-popup/jquery.magnific-popup.js?ver=4.4.2'></script> 
<script src='js/jquery.tubular.1.0.js?ver=4.4.2'></script> 
<script src='js/masonry.pkgd.min.js?ver=4.4.2'></script> 
<script src='js/imagesloaded.pkgd.min.js?ver=4.4.2'></script> 
<script src='js/comment-reply.min.js?ver=4.4.2'></script> 
<script src='js/wp-embed.min.js?ver=4.4.2'></script> 
<script src='js/underscore.min.js?ver=1.6.0'></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src='js/wp-util.min.js?ver=4.4.2'></script> 
<script src='plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=2.5.2'></script>
<style type="text/css">
	.sortable li{
		padding:8px 30px;
		font-size: 24px;
		font-family: sans-serif;
		text-transform: uppercase;

	}
	.sortable input[type=checkbox]{
		
  left: 0;
  height: 20px;
  width: 25px;
  background-color: #eee;
	}
	.sortable input:hover{
	background-color: #ccc;
	}
	.sortable input:checked{
	background-color: #2196F3;
	}
</style>
<script>

	// $(function() {
 //    $(".sortable").sortable();
 // })
	/* <![CDATA[ */
	var megamenu = {"effect":{"fade":{"in":{"animate":{"opacity":"show"}},"out":{"animate":{"opacity":"hide"}}},"slide":{"in":{"animate":{"height":"show"},"css":{"display":"none"}},"out":{"animate":{"height":"hide"}}}},"fade_speed":"fast","slide_speed":"fast","timeout":"300"};
	/* ]]> */

	function datecheck()
		{
			var date1, date2;
         date1 = new Date();
         date2 = new Date( "Feb 1, 2019 21:20:15" );
         if (date1 > date2) {
            window.location.href="meritlist.php";
         } else {
            alert("Merit List will be uploaded soon");
         }
		}

		function datecheck1()
		{
			var date1, date2 , date3;
         date1 = new Date();
         date2 = new Date( "Feb 1, 2019 21:20:15" );
         date3 = new Date( "Feb 10, 2019 21:20:15" );
         if (date1 > date2 && date1 < date3) {
            window.location.href="fyform.html";
         } else {
            alert("Registration Form is closed");
         }
		}
		function datecheck2()
		{
			var date1, date2;
         date1 = new Date();
         date2 = new Date( "Feb 1, 2019 21:20:15" );
         if (date1 > date2) {
            window.location.href="rounddetails.php";
         } else {
            alert("Merit List will be uploaded soon");
         }
		}

	
</script>
</body>
</html>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Megaw &#8211; Multi Purpose HTML Template</title>
<link rel='stylesheet' id='layerslider-css'  href='css/layerslider.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='ls-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='plugins/revslider/public/assets/css/settings.css?ver=5.0.9' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css'  href='plugins/woocommerce/assets/css/woocommerce-layout.css?ver=2.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=2.5.2' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='plugins/woocommerce/assets/css/woocommerce.css?ver=2.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='megamenu-css'  href='maxmegamenu/style.css?ver=762094' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-quick-view-css'  href='plugins/yith-woocommerce-quick-view/assets/css/yith-quick-view.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='plugins/woocommerce/assets/css/prettyPhoto.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css'  href='plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-main-css'  href='plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-font-awesome-css'  href='plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimp-for-wp-checkbox-css'  href='plugins/mailchimp-for-wp/assets/css/checkbox.min.css?ver=2.3.7' type='text/css' media='all' />
<link rel='stylesheet' id='wope-gfont-Montserrat-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-gfont-Open-Sans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-default-style-css'  href='style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-responsive-css'  href='responsive.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='flexslider-style-css'  href='js/flex-slider/flexslider.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-style-css'  href='js/magnific-popup/magnific-popup.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-font-awesome-css'  href='font-awesome/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-font-pe-icon-7-stroke-css'  href='pe-icon-7-stroke/css/pe-icon-7-stroke.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimp-for-wp-form-css'  href='plugins/mailchimp-for-wp/assets/css/form.min.css?ver=2.3.7' type='text/css' media='all' />
<script src='plugins/LayerSlider/static/js/greensock.js?ver=1.11.8'></script>
<script src='js/jquery/jquery.js?ver=3.0.0'></script>
<script src='js/jquery/jquery-migrate.min.js?ver=3.0.1'></script>
<script src='plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js?ver=5.3.2'></script>
<script src='plugins/LayerSlider/static/js/layerslider.transitions.js?ver=5.3.2'></script>
<script src='plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.0.9'></script>
<script src='plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.0.9'></script>
<script src='js/hoverIntent.min.js?ver=1.8.1'></script>
<script src='plugins/megamenu/js/maxmegamenu.js?ver=1.8.2'></script>

<link rel='stylesheet' href='css/shop.css' type='text/css' media='all' />

</head>


<body class="page page-template-default woocommerce-cart woocommerce-page">

<div id="background" style="">
	<div id="page" >
		<div id="header" class="header-text-white header-transparent heading-align-center " >
			<div class='header_bg'></div>
			<div class="header_content">
				<div id="top-bar">
					<div class="wrap">
						<div class="left-top-bar">
							<div class="contact-detail-line">
								<ul>
									<li><i class="fa fa-phone"></i> +91–022–2649 5808 </li>
									<li><i class="fa fa-envelope-o"></i> admission@tsec.edu </li>
									<li><i class="fa fa-clock-o"></i> 10AM - 5PM </li>
								</ul>
							</div>
						</div>
						<div class="right-top-bar">
							<div class="header-social"> <a target="_blank" href=" feed/ "><i class="fa fa-rss"></i></a> <a target="_blank"  href="#"><i class="fa fa-behance"></i></a> <a target="_blank"  href="#"><i class="fa fa-dribbble"></i></a> <a target="_blank"  href="#"><i class="fa fa-facebook"></i></a>
								<div class="cleared"></div>
							</div>
						</div>
						<div class="cleared"></div>
					</div>
				</div>
				<div id="top-bar-open">
					<div class="wrap">
						<div id="topbar-open"><i class="fa fa-angle-down"></i></div>
					</div>
				</div>
				<div class="wrap">
					<div class="left-header">
						<div class="site-logo">
							<h1> <a class="logo-image" href=""> <img class="logo-normal" alt="Megaw" src="images/tseclogo.png" /> <img class="logo-retina"   alt="Megaw" src="images/tseclogo.png" /> </a> </h1>
						</div>
					</div>
					<div class="right-header">
						<div class="main-menu">
							<div id="mega-menu-wrap-main-menu" class="mega-menu-wrap">
								<div class="mega-menu-toggle"></div>
								<ul id="mega-menu-main-menu" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="disabled" data-second-click="close" data-breakpoint="600">
									
									</li>
																		
								</ul>
							</div>
						</div>
						<!-- End Main Menu --> 
					</div>
					<div class="cleared"></div>
					<div id="toggle-menu-button"><i class="fa fa-align-justify"></i></div>
					
				</div>
				<!-- End Header Wrap --> 
				<!-- End Header --> 
				
				<!--Header Content for this page-->
				
				
			</div>
			<!-- End Header Content --> 
		</div>
		<!-- end Header -->
		<div class="cleared"></div>
		<div id="body" class="content-page">
			<div class="wrap">
				<div class="page-entry-content content">
					<div class="woocommerce">
						<form name="checkout" method="post" class="checkout woocommerce-checkout" action="fyform2.php" enctype="multipart/form-data">
							<div class="col2-set" id="customer_details">
							
							<div class="widget-entry">
							<div class="column1">
								<div class="wrap-no-fullwidth">
									<div class="section-widget-heading">
										<h3 class="section-widget-title" style="font-size: 32px;font-weight: bold;">ADMISSION FORM</h3>
									</div>
								</div>
							</div>
							<div class="cleared"></div>
								<div class="process-content content process-content-4 process_content_active">
									<h2 style="font-weight: bold;">Other Documents</h2>
									<div class="col-1">
										
										<div class="woocommerce-billing-fields">
																				
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" >
											<label  id="photo_label" style="color: #000;" id="layer_label"> Non Creamy Layer Certificate <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" >
											<input type="file" class="button" name="file_array[]" value="Upload" id="layer" required />	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;">
											<label  style="color: #000;" > Nationality Certificate <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" id="nationality" required/>	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label  style="color: #000;"> Migration Certificate <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label  style="color: #000;"> Transfer Certificate <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required />	
										</p>
										<div class="clear"></div>
										

													
										<div class="clear"></div>
										<div class="clear"></div>
											
												
										</div>
										
										
									</div>
									<div class="col-2">
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label  style="color: #000;"> Migration Affidavit <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label  style="color: #000;"> EBC Affidavit <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label  style="color: #000;"> Anti-Ragging Affidavit<span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										<div class="clear"></div>
										
										<div class="clear"></div>
										
										</div>
										
									</div>
									
								</div>
								<div class="cleared"></div>
								<p class="form-row">
										<input type="submit" class="button" name="submit" value="Submit" />
										
										<input type="button" onclick="back()" class="button" name="submit" value="Back" />
								</p>

							</div>
						</div>
						<div class="cleared"></div>


						<div class="cleared"></div>
					</div>
					<!-- end widget entry --> 
				</div>
				<div class="cleared"></div>
				
				</form>
				</div>
				</div>
				<div class="cleared"></div>
			</div>
		</div>
		
		<!-- footer start--><?php include 'footer.php' ?><!-- footer end-->
		<!-- End Footer Bottom --> 
	</div>
	<!-- End Page --> 
</div>
<!-- End Site Background -->

<div id="yith-quick-view-modal">
	<div class="yith-quick-view-overlay"></div>
	<div class="yith-wcqv-wrapper">
		<div class="yith-wcqv-main">
			<div class="yith-wcqv-head"> <a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a> </div>
			<div id="yith-quick-view-content" class="woocommerce single-product"></div>
		</div>
	</div>
</div>
<script src='plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=2.5.2'></script> 
<script src='plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script> 
<script src='plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=2.5.2'></script> 
<script src='plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1'></script> 
<script src='plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=2.5.2'></script> 
<script src='plugins/yith-woocommerce-quick-view/assets/js/frontend.js?ver=1.0'></script> 
<script src='plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.5'></script> 
<script src='plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.init.min.js?ver=2.5.2'></script> 
<script src='plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=4.4.2'></script> 
<script src='plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js?ver=2.0'></script> 
<script src='js/jquery/ui/core.min.js?ver=1.11.4'></script> 
<script src='js/jquery/ui/widget.min.js?ver=1.11.4'></script> 
<script src='js/jquery/ui/accordion.min.js?ver=1.11.4'></script> 
<script src='js/script.js?ver=4.4.2'></script> 
<script src='js/jquery.easing.1.3.js?ver=4.4.2'></script> 
<script src='js/jquery.isotope.min.js?ver=4.4.2'></script> 
<script src='js/flex-slider/jquery.flexslider-min.js?ver=4.4.2'></script> 
<script src='js/magnific-popup/jquery.magnific-popup.js?ver=4.4.2'></script> 
<script src='js/jquery.tubular.1.0.js?ver=4.4.2'></script> 
<script src='js/masonry.pkgd.min.js?ver=4.4.2'></script> 
<script src='js/imagesloaded.pkgd.min.js?ver=4.4.2'></script> 
<script src='js/comment-reply.min.js?ver=4.4.2'></script> 
<script src='js/wp-embed.min.js?ver=4.4.2'></script> 
<script src='js/underscore.min.js?ver=1.6.0'></script> 
<script src='js/wp-util.min.js?ver=4.4.2'></script> 
<script src='plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=2.5.2'></script>
<script>
	/* <![CDATA[ */
	var megamenu = {"effect":{"fade":{"in":{"animate":{"opacity":"show"}},"out":{"animate":{"opacity":"hide"}}},"slide":{"in":{"animate":{"height":"show"},"css":{"display":"none"}},"out":{"animate":{"height":"hide"}}}},"fade_speed":"fast","slide_speed":"fast","timeout":"300"};
	/* ]]> */
</script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script>
			
		$(document).ready(function(){
		  // Add smooth scrolling to all 

		  $("a").on('click', function(event) {
		  	
		    // Make sure this.hash has a value before overriding default behavior
		    if (this.hash !== "") {
		      // Prevent default anchor click behavior
		      event.preventDefault();

		      // Store hash
		      var hash = this.hash;

		      // Using jQuery's animate() method to add smooth page scroll
		      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		   
		        // Add hash (#) to URL when done scrolling (default click behavior)
		        window.location.hash = hash;
		      });
		    } // End if
		  });
			     
		});

		function back(){
			 window.history.back();
		}
	
 </script>


<?php

	$t=time();
	
	
	

$first_name=$_POST["first_name"];
$last_name=$_POST["last_name"];
$father_name=$_POST["father_name"];
$mother_name=$_POST["mother_name"];
$gender=$_POST["gender"];
$mother_tongue=$_POST["mother_tongue"];
$dob=$_POST["dob"];
$religion=$_POST["religion"];
$income=$_POST["income"];
$candidate_type=$_POST["candidate_type"];
$category=$_POST["category"];
$phtype=$_POST["phtype"];
$defencetype=$_POST["defencetype"];
$linguistic=$_POST["linguistic"];
$religious=$_POST["religious"];

$email=$_POST["email"];
$phone=$_POST["phone"];
$address_1=$_POST["address_1"];
$address_2=$_POST["address_2"];
$tel=$_POST["tel"];
$city=$_POST["city"];
$taluka=$_POST["taluka"];
$district=$_POST["district"];
$state=$_POST["state"];
$postcode=$_POST["postcode"];

$tenth=$_POST["tenth"];
$tenth_year=$_POST["tenth_year"];
$tenth_percent=$_POST["tenth_percent"];
$tenth_maths_percent=$_POST["tenth_maths_percent"];
$tenth_seat_no=$_POST["tenth_seat_no"];
$qualifying_exam=$_POST["qualifying_exam"];
$tw_year=$_POST["tw_year"];
$tw_total_percent=$_POST["tw_total_percent"];
$tw_maths_percent=$_POST["tw_maths_percent"];
$tw_phy_percent=$_POST["tw_phy_percent"];
$tw_chem_percent=$_POST["tw_chem_percent"];
$tw_eng_percent=$_POST["tw_eng_percent"];
$tw_subj_percent=$_POST["tw_subj_percent"];
$tw_seat_no=$_POST["tw_seat_no"];
$add_subject=$_POST["add_subject"];
$eligibility_percent=$_POST["eligibility_percent"];
$cet_roll_no=$_POST["cet_roll_no"];
$cet_score=$_POST["cet_score"];
$jee_roll_no=$_POST["jee_roll_no"];
$jee_score=$_POST["jee_score"];
$merit_no=$_POST["merit_no"];
$merit_marks=$_POST["merit_marks"];
$institute_code=$_POST["institute_code"];
$institute_name=$_POST["institute_name"];
$course_name=$_POST["course_name"];

$choicecode=$_POST["choicecode"];
$seattype=$_POST["seattype"];


	$link = mysqli_connect("localhost","root","","cap");
	
	if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
	
	// Check connection
	if($link === false){
		echo '<div class="col-md-11 work-right-w3-agileits">
						<h3><span>Unable to send Message.</span></h3></div>';
	}
	// Attempt insert query executio
	$sql = "INSERT INTO stud_details (app_id, fname, lname, fathername, mothername, gender, mothertongue, dob, religion, income, ctype, category,  phtype, defencetype, lminority, rminority, mobile, email, address1, address2, telephone, village, taluka, district, state, pincode, sscboard, sscpassyear, sscpercent, sscmaths,sscseatno, sscqualifyingexam, hscboard, hscpassyear, hscpercent, hscmaths, hscphysics, hscchemistry, hscenglish, hscsubject, hscseatno, hscadditional, eligiblepercent, cetrollno, cetscore, jeerollno, jeescore, meritno, meritmarks, instcode, coursename, seattype, choicecode, file)  
		VALUES (NULL,'".$first_name."', '".$last_name."', '".$father_name."', '".$mother_name."',  '".$gender."',  '".$mother_tongue."',  '".$dob."',  '".$religion."',  '".$income."' ,  '".$candidate_type."',  '".$category."',  '".$phtype."',  '".$defencetype."',  '".$linguistic."',  '".$religious."',  '".$phone."', '".$email."',   '".$address_1."',  '".$address_2."', '".$tel."',  '".$city."',  '".$taluka."',  '".$district."',  '".$state."',  '".$postcode."',  '".$tenth."',  '".$tenth_year."',  '".$tenth_percent."',  '".$tenth_maths_percent."',  '".$tenth_seat_no."' ,  '".$qualifying_exam."',  '".$tw_year."',  '".$tw_total_percent."',  '".$tw_maths_percent."',  '".$tw_phy_percent."',  '".$tw_chem_percent."',  '".$tw_eng_percent."',  '".$tw_subj_percent."',  '".$tw_seat_no."',  '".$add_subject."',  '".$eligibility_percent."',  '".$cet_roll_no."',  '".$cet_score."',  '".$jee_roll_no."',  '".$jee_score."',  '".$merit_no."',  '".$merit_marks."',  '".$institute_code."',  '".$institute_name."',  '".$course_name."',  '".$seattype."',  '".$choicecode."',  '".$t."')";

	if(mysqli_query($link, $sql)){
		// $to_email = $username;
		// $subject = 'TSEC Registeration';
		// $message = 'Thank you for registering. Please read our brochure for more details.';
		// $headers = 'From: tsec@gmail.com';
		// mail($to_email,$subject,$message,$headers);
		echo '<div class="col-md-11 work-right-w3-agileits">
						<h3><span>Registered Successfully, Please Login now.</span></h3></div>';
	} else{
		echo '<div class="col-md-11 work-right-w3-agileits">
						<h3><span>Message sending failed.</span></h3></div>';
	}
	 
	// Close connection
	mysqli_close($link);

	if(strcasecmp($state, 'Maharashtra')==0)
	{
		echo "<script>
		document.getElementById('nationality').disabled=true;
		document.getElementById('nationality_label').style.color='gray';
		</script>";	
	}
	echo "<script>
	document.getElementById('layer').disabled=true;
	document.getElementById('layer_label').style.color='gray';
	</script>";

    
 
   if(isset($_FILES['file_array'])){
    $name_array = $_FILES['file_array']['name'];
    $tmp_name_array = $_FILES['file_array']['tmp_name'];
    $type_array = $_FILES['file_array']['type'];
    $size_array = $_FILES['file_array']['size'];
    $error_array = $_FILES['file_array']['error'];
    echo "string1";
    for($i = 0; $i < count($tmp_name_array); $i++){
        if(move_uploaded_file($tmp_name_array[$i], "uploads/".$t."".$i.".pdf")){
            echo $name_array[$i]." upload is complete<br>";
        } else {
            echo "move_uploaded_file function failed for ".$name_array[$i]."<br>";
        }
    }
}
echo "string3";
?>
</body>
</html>
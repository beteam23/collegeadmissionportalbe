	<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>TSEC Admission Portal</title>
<link rel='stylesheet' id='layerslider-css'  href='css/layerslider.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='ls-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='plugins/revslider/public/assets/css/settings.css?ver=5.0.9' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css'  href='plugins/woocommerce/assets/css/woocommerce-layout.css?ver=2.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=2.5.2' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='plugins/woocommerce/assets/css/woocommerce.css?ver=2.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='megamenu-css'  href='maxmegamenu/style.css?ver=762094' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-quick-view-css'  href='plugins/yith-woocommerce-quick-view/assets/css/yith-quick-view.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='plugins/woocommerce/assets/css/prettyPhoto.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css'  href='plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-main-css'  href='plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-font-awesome-css'  href='plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimp-for-wp-checkbox-css'  href='plugins/mailchimp-for-wp/assets/css/checkbox.min.css?ver=2.3.7' type='text/css' media='all' />
<link rel='stylesheet' id='wope-gfont-Montserrat-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-gfont-Open-Sans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-default-style-css'  href='style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-responsive-css'  href='responsive.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='flexslider-style-css'  href='js/flex-slider/flexslider.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-style-css'  href='js/magnific-popup/magnific-popup.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-font-awesome-css'  href='font-awesome/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-font-pe-icon-7-stroke-css'  href='pe-icon-7-stroke/css/pe-icon-7-stroke.css?ver=4.4.2' type='text/css' media='all' />
<script src='plugins/LayerSlider/static/js/greensock.js?ver=1.11.8'></script>
<script src='js/jquery/jquery.js?ver=3.0.0'></script>
<script src='js/jquery/jquery-migrate.min.js?ver=3.0.1'></script>
<script src='plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js?ver=5.3.2'></script>
<script src='plugins/LayerSlider/static/js/layerslider.transitions.js?ver=5.3.2'></script>
<script src='plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.0.9'></script>
<script src='plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.0.9'></script>
<script src='js/hoverIntent.min.js?ver=1.8.1'></script>
<script src='plugins/megamenu/js/maxmegamenu.js?ver=1.8.2'></script>
</script>
<link rel='stylesheet' href='css/custom.css' type='text/css' media='all' />
</head>
<body class="home page page-template-default">
<div id="background" style="">
	<div id="page" >
		<div id="header" class="header-text-white header-transparent heading-align-center " >
			<div class='header_bg'></div>
			<div class="header_content">
				<div id="top-bar">
					<div class="wrap">
						<div class="left-top-bar">
							<div class="contact-detail-line">
								<ul>
									<li><i class="fa fa-phone"></i> +91–022–2649 5808 </li>
									<li><i class="fa fa-envelope-o"></i> admission@tsec.edu </li>
									<li><i class="fa fa-clock-o"></i> 10AM - 5PM </li>
								</ul>
							</div>
						</div>
						<div class="right-top-bar">
							<div class="header-social"> <a target="_blank" href=" feed/ "><i class="fa fa-rss"></i></a> <a target="_blank"  href="#"><i class="fa fa-behance"></i></a> <a target="_blank"  href="#"><i class="fa fa-dribbble"></i></a> <a target="_blank"  href="#"><i class="fa fa-facebook"></i></a>
								<div class="cleared"></div>
							</div>
							
						</div>
						<div class="cleared"></div>
					</div>
				</div>
				<div id="top-bar-open">
					<div class="wrap">
						<div id="topbar-open"><i class="fa fa-angle-down"></i></div>
					</div>
				</div>
				<div class="wrap">
					<div class="left-header">
						<div class="site-logo">
							<h1> <a class="logo-image" href=""> <img class="logo-normal" alt="Megaw" src="images/tseclogo.png" /> <img class="logo-retina"   alt="Megaw" src="images/tseclogo.png" /> </a> </h1>
						</div>
					</div>
		
					<div class="right-header">
						<div class="main-menu">
							<div id="mega-menu-wrap-main-menu" class="mega-menu-wrap">
								<div class="mega-menu-toggle"></div>
								<ul id="mega-menu-main-menu" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="" data-second-click="close" data-breakpoint="600">
									<li class='mega-menu-item mega-menu-item-type-custom mega-current-menu-item'><a href="#">HOME</a></li>
									<li class='mega-menu-item mega-menu-item-type-custom'><a href="loginregister.php">Register / Login</a></li>
									<li class='mega-menu-item mega-menu-item-type-custom'><a href="contactus.php">Contact us</a></li>
									<li class='mega-menu-item mega-menu-item-type-custom'><a href="aboutus.php">About us</a></li>
								</ul>
							</div>
						</div>
						<!-- End Main Menu --> 
					</div>
					<div class="cleared"></div>
					<div id="toggle-menu-button"><i class="fa fa-align-justify"></i></div>
					<div class="toggle-menu">
						<div class="menu-toggle-menu-container">
							<ul id="menu-toggle-menu" class="menu">
								<li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom  mega-menu-item-has-children mega-menu-item-18 mega-align-bottom-left mega-menu-flyout'><a href="#.php">HOME</a>
								</li>
								<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-has-children mega-menu-item-1360 mega-align-bottom-left mega-menu-flyout'><a href="loginregister.php">Register / Login</a>
								</li>
								<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-has-children mega-menu-item-1360 mega-align-bottom-left mega-menu-flyout'><a href="contactus.php">Contact us</a>
								</li>
								<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-has-children mega-menu-item-1360 mega-align-bottom-left mega-menu-flyout'><a href="aboutus.php">About us</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
					
				<!-- End Header Wrap -->  
				<!-- End Header --> 
				
				<!--Header Content for this page-->
				
				
			</div>
			<!-- End Header Content --> 
		</div>
		<!-- end Header -->
		<div class="cleared"></div>
		<!-- BEGIN CONTENT -->
		<div id="body">
			<div id="content-section1" class="content-section black-text white-bg top-spacing-no bottom-spacing-no title-spacing-medium align-center " style="">
				<div class="no-wrap">
					<div class="content-column1">
						<div class="widget-entry">
							<div class="column1">
								<div class="content content-box-content">
									<div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper fullscreen-container" style=" padding:0px;"> 
										<!-- START REVOLUTION SLIDER 5.0.9 fullscreen mode -->
										<div id="rev_slider_3_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.0.9">
											<ul>
												<!-- SLIDE  -->
												<li data-index="rs-28" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="images/1_low-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-description=""> 
													<!-- MAIN IMAGE --> 
													<img src="images/home-banner.jpg"  alt=""  width="1400" height="882" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina> 
													<!-- LAYERS --> 
													
													<!-- LAYER NR. 1 -->
													<div class="tp-caption Megaw-Headline1   tp-resizeme rs-parallaxlevel-0" 
														id="slide-28-layer-1" 
														data-x="['center','center','center','center']" data-hoffset="['7','7','7','3']" 
														data-y="['top','top','top','top']" data-voffset="['203','203','203','159']" 
														data-fontsize="['60','60','40','24']"
														data-width="none"
														data-height="none"
														data-whitespace="nowrap"
														data-transform_idle="o:1;"
														data-transform_in="x:-50px;opacity:0;s:300;e:Power2.easeInOut;" 
														data-transform_out="opacity:0;s:300;s:300;" 
														data-start="500" 
														data-splitin="none" 
														data-splitout="none" 
														data-responsive_offset="on" 
														style="z-index: 5; white-space: nowrap;">TSEC ADMISSION PORTAL
													</div>
													
													<!-- LAYER NR. 3 -->
													<div class="tp-caption Megaw-Button rev-btn  rs-parallaxlevel-0" 
														id="slide-28-layer-3" 
														data-x="['center','center','center','center']" data-hoffset="['9','9','10','10']" 
														data-y="['top','top','top','top']" data-voffset="['398','398','366','289']" 
														data-width="none"
														data-height="none"
														data-whitespace="nowrap"
														data-transform_idle="o:1;"
														data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:200;e:Power0.easeOut;"
														data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);background-color:#ffffff;cursor:pointer;"
														data-transform_in="y:50px;opacity:0;s:300;e:Power2.easeInOut;" 
														data-transform_out="opacity:0;s:300;s:300;" 
														data-start="1500" 
														data-splitin="none" 
														data-splitout="none" 
														data-responsive_offset="on" 
														data-responsive="off"
														style="z-index: 7; white-space: nowrap;">LEARN MORE
													</div>
												</li>
												
											</ul>
										</div>
										<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss=".tp-caption.Megaw-Headline1,.Megaw-Headline1{color:rgba(255,255,255,1.00);font-size:60px;line-height:60px;font-weight:700;font-style:normal;font-family:Montserrat;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.Megaw-Text,.Megaw-Text{color:rgba(255,255,255,1.00);font-size:30px;line-height:36px;font-weight:300;font-style:normal;font-family:Open Sans;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.Megaw-Button,.Megaw-Button{color:rgba(255,255,255,1.00);font-size:18px;line-height:18px;font-weight:400;font-style:normal;font-family:Montserrat;padding:13px 35px 13px 35px;text-decoration:none;text-align:left;background-color:transparent;border-color:rgba(255,255,255,1.00);border-style:solid;border-width:2px;border-radius:0px 0px 0px 0px}.tp-caption.Megaw-Button:hover,.Megaw-Button:hover{color:rgba(46,46,46,1.00);text-decoration:none;background-color:rgba(255,255,255,1.00);border-color:rgba(255,255,255,1.00);border-style:solid;border-width:2px;border-radius:0px 0px 0px 0px}";
											if(htmlDiv) {
												htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
											}
											else{
												var htmlDiv = document.createElement("div");
												htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
												document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
											}
										</script> 
										<script>
											/******************************************
											-	PREPARE PLACEHOLDER FOR SLIDER	-
											******************************************/
								
											var setREVStartSize=function(){
												try{var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
													e.c = jQuery('#rev_slider_3_1');
													e.responsiveLevels = [1240,1024,778,480];
													e.gridwidth = [1240,1024,778,480];
													e.gridheight = [600,600,500,400];
															
													e.sliderLayout = "fullscreen";
													e.fullScreenAutoWidth='off';
													e.fullScreenAlignForce='off';
													e.fullScreenOffsetContainer= '';
													e.fullScreenOffset='';
													if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
												}catch(d){console.log("Failure at Presize of Slider:"+d)}
											};
														
												
											setREVStartSize();
											function revslider_showDoubleJqueryError(sliderID) {
													var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
													errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
													errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
													errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
													errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>"
														jQuery(sliderID).show().html(errorMessage);
												}
														var tpj=jQuery;
											
											var revapi3;
											tpj(document).ready(function() {
												if(tpj("#rev_slider_3_1").revolution == undefined){
													revslider_showDoubleJqueryError("#rev_slider_3_1");
												}else{
													revapi3 = tpj("#rev_slider_3_1").show().revolution({
														sliderType:"standard",
														jsFileLocation:"plugins/revslider/public/assets/js/",
														sliderLayout:"fullscreen",
														dottedOverlay:"none",
														delay:9000,
														navigation: {
															keyboardNavigation:"off",
															keyboard_direction: "horizontal",
															mouseScrollNavigation:"off",
															onHoverStop:"on",
															touch:{
																touchenabled:"on",
																swipe_threshold: 75,
																swipe_min_touches: 50,
																swipe_direction: "horizontal",
																drag_block_vertical: false
															}
															,
															
														},
														responsiveLevels:[1240,1024,778,480],
														gridwidth:[1240,1024,778,480],
														gridheight:[600,600,500,400],
														lazyType:"smart",
														parallax: {
															type:"mouse",
															origo:"slidercenter",
															speed:2000,
															levels:[2,3,4,5,6,7,12,16,10,50],
														},
														shadow:0,
														spinner:"off",
														stopLoop:"off",
														stopAfterLoops:-1,
														stopAtSlide:-1,
														shuffle:"off",
														autoHeight:"off",
														fullScreenAutoWidth:"off",
														fullScreenAlignForce:"off",
														fullScreenOffsetContainer: "",
														fullScreenOffset: "",
														disableProgressBar:"on",
														hideThumbsOnMobile:"off",
														hideSliderAtLimit:0,
														hideCaptionAtLimit:0,
														hideAllCaptionAtLilmit:0,
														debugMode:false,
														fallbacks: {
															simplifyAll:"off",
															nextSlideOnWindowFocus:"off",
															disableFocusListener:false,
														}
													});
												}
											});	/*ready*/
										</script> 
										
									</div>
									<!-- END REVOLUTION SLIDER -->
								</div>
							</div>
							<div class="cleared"></div>
							<div class="cleared"></div>
						</div>
						<!-- end widget entry --> </div>
					<div class="cleared"></div>
				</div>
				<!-- end wrap-column no-wrap --> 
				
			</div>
			<!-- end content section -->
			
			
			<div id="content-section9" class="content-section black-text white-bg top-spacing-medium bottom-spacing-medium title-spacing-medium align-center " style="">
				<div class="wrap-column">
					<div class="column1">
						<div class="wrap">
							<div class="content-section-heading">
								<h1 class="content-section-title"> Recent Updates </h1>
							</div>
						</div>
					</div>
					<div class="cleared"></div>
				</div>
				<div class="wrap-column">
					<div class="content-column1">
						<div class="widget-entry " 	>
							<div class="widget-column2-3 " >
								<div class="team-item widget-element-bottom team-style-1"  >
									<div class="team-detail" style="background-color: #0d47a1;padding: 30px 30px;">
										<div class="team-heading">
											
											<div class="team-name" style="color: #fff;font-size: 30px;margin-bottom: 40px;margin-top: 30px;"> IMPORTANT NOTICE </div>
										</div>
										<div class="team-content">
											<p><a target="_blank"  title="github" class="social-icons social-github" href="#" ><i style="color: #fff;" class="fa fa-info"></i></a>Ph.D. Admission Notice 2019 <b style="color: gold;"> new</b></p>

											<p><a target="_blank"  title="github" class="social-icons social-github" href="#" ><i style="color: #fff;" class="fa fa-info"></i></a>Ph.D. Course Work Interview Notification 2019</p>

											<p><a target="_blank"  title="github" class="social-icons social-github" href="#" ><i style="color: #fff;" class="fa fa-info"></i></a>Ph.D. Admission Notification 2019</p>

											<p><a target="_blank"  title="github" class="social-icons social-github" href="#" ><i style="color: #fff;" class="fa fa-info"></i></a>Provisional Merit List For Direct Second Year Admission <b style="color: gold;"> new</b></p>

											<p><a target="_blank"  title="github" class="social-icons social-github" href="#" ><i style="color: #fff;" class="fa fa-info"></i></a>DSE Admission Notification</p>

											<p><a target="_blank"  title="github" class="social-icons social-github" href="#" ><i style="color: #fff;" class="fa fa-info"></i></a>Admission Against Cancellation ( till cut off date:- 31st August 2018)</p>

											<p><a target="_blank"  title="github" class="social-icons social-github" href="#" ><i style="color: #fff;" class="fa fa-info"></i></a>Fee Structure (Direct Second Year) -2018-2019</p>

											<p><a target="_blank"  title="github" class="social-icons social-github" href="#" ><i style="color: #fff;" class="fa fa-info"></i></a>Fee Structure (First year) 2018-19</p>
										</div>
										
									</div>
								</div>
							</div>
							<div class="widget-column1-3 ">
								<div class="team-item widget-element-bottom team-style-1">
									<div class="team-detail" style="background-color: #fff;border:solid 2px #0d47a1;">
										
										<div class="team-content" style="text-align: center;">
											<div class="download-file">
												<p>Anti-Ragging Form</p>
												<a target="_blank" class="large-button white-button " href="#">Download</a>
											</div>
											<div class="download-file">
												<p>Transcript Form</p>
												<a target="_blank" class="large-button white-button " href="#">Download</a>
											</div>
											<div class="download-file">
												<p>Online KT Form</p>
												<a target="_blank" class="large-button white-button " href="#">Download</a>
											</div>
											<div class="download-file">
												<p>Fee Structure</p>
												<a target="_blank" class="large-button white-button " href="#">Download</a>
											</div>
											<div class="download-file">
												<p>Important Dates</p>
												<a target="_blank" class="large-button white-button " href="#">Download</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="cleared"></div>
							<div class="cleared"></div>
						</div>
						<!-- end widget entry --> </div>
					<div class="cleared"></div>
				</div>
				<!-- end wrap-column no-wrap --> 
				
			</div>
			<!-- end content section -->
			
			<div id="content-section10" class="content-section black-text gray-bg top-spacing-no bottom-spacing-no title-spacing-medium align-left " >
				<div class="wrap-column">
					<div class="content-column2_1">
						<div class="widget-entry">
							<div class="column1">
								
								<div class="gallery "style="margin-bottom: 100px;">
									<div class="gallery-image-lightbox gallery-center " style="">
										<div class="gallery-image-bg"></div>
										<img title="Gallery 01" src="images/ad1.jpg" alt="promote_box_041"> </div>
								</div>
							</div>
							<div class="cleared"></div>
							<div class="cleared"></div>
						</div>
						<!-- end widget entry --> </div>
						<div class="content-column2_1">
						<div class="widget-entry">
							<div class="column1">
								
								<div class="gallery "style="margin-bottom: 100px;">
									<div class="gallery-image-lightbox gallery-center " style="">
										<div class="gallery-image-bg"></div>
										<img title="Gallery 01" src="images/ad2.jpg" alt="promote_box_041"> </div>
								</div>
							</div>
							<div class="cleared"></div>
							<div class="cleared"></div>
						</div>
						<!-- end widget entry --> </div>
				</div>
			</div>
			<!-- end content section -->
			<div class="cleared"></div>
							<div class="cleared"></div>
			
		</div>
		<!-- footer start--><?php include 'footer.php' ?><!-- footer end-->
	</div>
	<!-- End Page --> 
</div>
<!-- End Site Background -->

<div id="yith-quick-view-modal">
	<div class="yith-quick-view-overlay"></div>
	<div class="yith-wcqv-wrapper">
		<div class="yith-wcqv-main">
			<div class="yith-wcqv-head"> <a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a> </div>
			<div id="yith-quick-view-content" class="woocommerce single-product"></div>
		</div>
	</div>
</div>
<script src='js/jquery/ui/core.min.js'></script> 
<script src='js/jquery/ui/widget.min.js'></script> 
<script src='js/jquery/ui/accordion.min.js'></script> 
<script src='js/script.js'></script> 
<script src='js/jquery.isotope.min.js'></script> 
<script>
	
	/* <![CDATA[ */
	var megamenu = {"effect":{"fade":{"in":{"animate":{"opacity":"show"}},"out":{"animate":{"opacity":"hide"}}},"slide":{"in":{"animate":{"height":"show"},"css":{"display":"none"}},"out":{"animate":{"height":"hide"}}}},"fade_speed":"fast","slide_speed":"fast","timeout":"300"};
	/* ]]> */
</script>
</body>
</html>
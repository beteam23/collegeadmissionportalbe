<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Megaw &#8211; Multi Purpose HTML Template</title>
<link rel='stylesheet' id='layerslider-css'  href='css/layerslider.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='ls-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='plugins/revslider/public/assets/css/settings.css?ver=5.0.9' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css'  href='plugins/woocommerce/assets/css/woocommerce-layout.css?ver=2.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=2.5.2' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='plugins/woocommerce/assets/css/woocommerce.css?ver=2.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='megamenu-css'  href='maxmegamenu/style.css?ver=762094' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-quick-view-css'  href='plugins/yith-woocommerce-quick-view/assets/css/yith-quick-view.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='plugins/woocommerce/assets/css/prettyPhoto.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css'  href='plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-main-css'  href='plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-font-awesome-css'  href='plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimp-for-wp-checkbox-css'  href='plugins/mailchimp-for-wp/assets/css/checkbox.min.css?ver=2.3.7' type='text/css' media='all' />
<link rel='stylesheet' id='wope-gfont-Montserrat-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-gfont-Open-Sans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-default-style-css'  href='style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-responsive-css'  href='responsive.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='flexslider-style-css'  href='js/flex-slider/flexslider.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-style-css'  href='js/magnific-popup/magnific-popup.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-font-awesome-css'  href='font-awesome/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-font-pe-icon-7-stroke-css'  href='pe-icon-7-stroke/css/pe-icon-7-stroke.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimp-for-wp-form-css'  href='plugins/mailchimp-for-wp/assets/css/form.min.css?ver=2.3.7' type='text/css' media='all' />

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src='plugins/LayerSlider/static/js/greensock.js?ver=1.11.8'></script>
<script src='js/jquery/jquery.js?ver=3.0.0'></script>
<script src='js/jquery/jquery-migrate.min.js?ver=3.0.1'></script>
<script src='plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js?ver=5.3.2'></script>
<script src='plugins/LayerSlider/static/js/layerslider.transitions.js?ver=5.3.2'></script>
<script src='plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.0.9'></script>
<script src='plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.0.9'></script>
<script src='js/hoverIntent.min.js?ver=1.8.1'></script>
<script src='plugins/megamenu/js/maxmegamenu.js?ver=1.8.2'></script>
<link rel='stylesheet' href='css/shop.css' type='text/css' media='all' />
</head>
<body class="page page-template-default woocommerce-cart woocommerce-page">
<div id="background" style="">
	<div id="page" >
		<div id="header" class="header-text-white header-transparent heading-align-center " >
			<div class='header_bg'></div>
			<div class="header_content">
				<div id="top-bar">
					<div class="wrap">
						<div class="left-top-bar">
							<div class="contact-detail-line">
								<ul>
									<li><i class="fa fa-phone"></i> +91–022–2649 5808 </li>
									<li><i class="fa fa-envelope-o"></i> admission@tsec.edu </li>
									<li><i class="fa fa-clock-o"></i> 10AM - 5PM </li>
								</ul>
							</div>
						</div>
						<div class="right-top-bar">
							<div class="header-social"> <a target="_blank" href=" feed/ "><i class="fa fa-rss"></i></a> <a target="_blank"  href="#"><i class="fa fa-behance"></i></a> <a target="_blank"  href="#"><i class="fa fa-dribbble"></i></a> <a target="_blank"  href="#"><i class="fa fa-facebook"></i></a>
								<div class="cleared"></div>
							</div>
						</div>
						<div class="cleared"></div>
					</div>
				</div>
				<div id="top-bar-open">
					<div class="wrap">
						<div id="topbar-open"><i class="fa fa-angle-down"></i></div>
					</div>
				</div>
				<div class="wrap">
					<div class="left-header">
						<div class="site-logo">
							<h1> <a class="logo-image" href=""> <img class="logo-normal" alt="Megaw" src="images/tseclogo.png" /> <img class="logo-retina"   alt="Megaw" src="images/tseclogo.png" /> </a> </h1>
						</div>
					</div>
					<div class="right-header">
						<div class="main-menu">
							<div id="mega-menu-wrap-main-menu" class="mega-menu-wrap">
								<div class="mega-menu-toggle"></div>
								<ul id="mega-menu-main-menu" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="disabled" data-second-click="close" data-breakpoint="600">
									<li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-current-menu-item mega-current_page_item mega-menu-item-home mega-current-menu-ancestor mega-current-menu-parent mega-menu-item-has-children mega-menu-item-18 mega-align-bottom-left mega-menu-flyout'><a href="userhome.php">Home</a>
										
									</li>
									<li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-menu-item-19 mega-align-bottom-left mega-menu-flyout'><a href="register.html">Logout</a>
										
									</li>

																		
								</ul>
							</div>
						</div>
						<!-- End Main Menu --> 
					</div>
					<div class="cleared"></div>
					<div id="toggle-menu-button"><i class="fa fa-align-justify"></i></div>
					<div class="toggle-menu">
						<div class="menu-toggle-menu-container">
							<ul id="menu-toggle-menu" class="menu">
								<li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom  mega-menu-item-has-children mega-menu-item-18 mega-align-bottom-left mega-menu-flyout'><a href="userhome.php">Home</a>
									</li>
									<li class='mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-menu-item-has-children mega-menu-item-1360 mega-align-bottom-left mega-menu-flyout'><a href="index.php">Logout</a>
									</li>
							</ul>
						</div>
					</div>
				</div>
					
				<!-- End Header Wrap -->  
				<!-- End Header --> 
				
				<!--Header Content for this page-->
				
				
			</div>
			<!-- End Header Content --> 
		</div>
		<!-- end Header -->
		<div class="cleared"></div>
		<div id="body" class="content-page">
			<div class="wrap">
				<div class="page-entry-content content">
					<div class="woocommerce">
						<form name="checkout" method="post" class="checkout woocommerce-checkout" action="fyform1.php" enctype="multipart/form-data">
							<div class="col2-set" id="customer_details">
							
							<div class="widget-entry">
							<div class="column1">
								<div class="wrap-no-fullwidth">
									<div class="section-widget-heading">
										<h3 class="section-widget-title" style="font-size: 32px;font-weight: bold;">ADMISSION FORM</h3>
									</div>
								</div>
							</div>
							<div class="cleared"></div>
							<div class="process process_container_3" style="margin-top: 0px;">
								<div class="process_row" style="pointer-events: none;">
									<div class="process_cell percent_column4  process_active">
										<div class="process-item widget-element-bottom process_style_3 process_active  process-tab-1">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 1 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name" style="font-weight: bold;"> PERSONAL </div>
											</div>
											<span class="process-tab-id">1</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4">
										<div class="process-item widget-element-bottom process_style_3 process-tab-2">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 2 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> CONTACT & ADDRESS </div>
											</div>
											<span class="process-tab-id">2</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4  ">
										<div class="process-item widget-element-bottom process_style_3   process-tab-3">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 3 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> ACADEMIC </div>
											</div>
											<span class="process-tab-id">3</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4 column-last ">
										<div class="process-item widget-element-bottom process_style_3  process_last process-tab-4">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 4 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> OTHER  & DOCUMENTS </div>
											</div>
											<span class="process-tab-id">4</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="cleared"></div>
								</div>
								<div class="process-content content process-content-1 process_content_active">
									<h2 style="font-weight: bold;">PERSONAL DETAILS </h2>
										<div class="col-1">
										<div class="woocommerce-billing-fields">
											
											<p class="form-row form-row form-row-first validate-required" id="first_name_field">
												<input type="text" class="input-text " name="first_name" id="first_name" placeholder="First Name"  value=""  required pattern="[a-zA-Z\s]{3-20}" />
											</p>
											<p class="form-row form-row form-row-last validate-required" id="last_name_field">
												<input type="text" class="input-text " name="last_name" id="last_name" placeholder="Last Name"  value=""  required pattern="[a-zA-Z\s]{3-20}"/>
											</p>
											<p class="form-row form-row form-row-first validate-required" id="father_name_field">
												<input type="text" class="input-text " name="father_name" id="father_name" placeholder="Father's Name"  value=""  required pattern="[a-zA-Z\s]{3-20}"/>
											</p>
											<p class="form-row form-row form-row-last validate-required" id="mother_name_field">
												<input type="text" class="input-text " name="mother_name" id="mother_name" placeholder="Mother's Name"  value="" required pattern="[a-zA-Z\s]{3-20}" />
											</p>
											
											<div class="clear"></div>
											<p class="form-row form-row form-row-first validate-required" id="gender_field">
												<select name="gender" id="gender" class="gender" required >
													<option value="" disabled>Select a Gender</option>
													<option value="Male" >Male</option>
													<option value="Female" >Female</option>
													<option value="Other" >Other</option>
												</select>
											</p>
											
											<p class="form-row form-row form-row-last validate-required" id="mothertongue_field">
												<input type="text" class="input-text " name="mother_tongue" id="mother_tongue" placeholder="Mother Tongue"  value="" required pattern="[a-zA-Z\s]{3-20}"/>
											</p>
											<div class="clear"></div>

											
											<p class="form-row form-row form-row-first validate-required" style="height: 48px;text-align: right;font-size: 20px;">
											<label for="dob" class="">Date of Birth <abbr class="required" title="required"></abbr></label>
											</p>
											<p class="form-row form-row form-row-last validate-required" id="dob_field">
												<input type="date" class="input-date " name="dob" id="dob" placeholder="DOB"  value=""  style="width: 95%;" required />
											</p>
											<div class="clear"></div>
											<p class="form-row form-row form-row-first validate-required" id="religion_field">
												<input type="text" class="input-text " name="religion" id="religion" placeholder="Religion"  value=""  required pattern="[a-zA-Z\s]{3-20}"/>
											</p>
											<p class="form-row form-row form-row-last validate-required" id="income_field">
												<input type="text" class="input-text " name="income" id="income" placeholder="Annual Family Income"  value="" required pattern="[0-9]{3-8}"/>
											</p>
											<div class="clear"></div>
												
										</div>
									</div>
									<div class="col-2">
										<div class="woocommerce-billing-fields">
											<p class="form-row form-row form-row-first validate-required" id="candidatetype_field">
												<select name="candidate_type" id="candidate_type" class="gender" required >
													<option value=""  disabled>Candidate type</option>
													<option value="cap" >CAP</option>
													<option value="against" >Against CAP</option>
													<option value="institue" >Institue</option>
												</select>
											</p>											
											<p class="form-row form-row form-row-last validate-required" id="category_field">
												<input type="text" class="input-text " name="category" id="category" placeholder="Category"  value=""  required pattern="[a-zA-Z\s]{3-20}"/>
											</p>
											<div class="clear"></div>
											<p class="form-row form-row form-row-first validate-required" id="phtype_field">
												<input type="text" class="input-text " name="phtype" id="phtype" placeholder="PH type"  value=""  required pattern="[a-zA-Z\s]{3-20}" />
											</p>
											<p class="form-row form-row form-row-last validate-required" id="defence_field">
												<input type="text" class="input-text " name="defencetype" id="defencetype" placeholder="Defence type"  value=""  required pattern="[a-zA-Z\s]{3-20}"/>
											</p>
											<p class="form-row form-row form-row-first validate-required" id="linguistic_field">
												<input type="text" class="input-text " name="linguistic" id="linguistic" placeholder="Linguistic Minority"  value=""  required pattern="[a-zA-Z\s]{3-20}" />
											</p>
											<p class="form-row form-row form-row-last validate-required" id="religious_field">
												<input type="text" class="input-text " name="religious" id="religious" placeholder="Religious Minority	"  value="" required pattern="[a-zA-Z\s]{3-20}" />
											</p>
											<p>(write NA if the field is not applicable.)</p>
											<div class="clear"></div>
											<div class="clear"></div>
											
										</div>
									</div>
								</div>
							</div>						

							<div class="cleared"></div>
							<p class="form-row">
													<a type="button" class="next" name="next" href="#acad" >Next</a>
												</p>


							<div class="process process_container_3" id="cont">
								<div class="process_row" style="pointer-events: none;">
									<div class="process_cell percent_column4  ">
										<div class="process-item widget-element-bottom process_style_3   process-tab-1">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 1 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> PERSONAL </div>
											</div>
											<span class="process-tab-id">1</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4 process_active" >
										<div class="process-item widget-element-bottom process_style_3 process_active process-tab-2">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 2 </div>
											</div>
											<div class="process-bottom"> 
												<div class="process-name" style="font-weight: bold;"> CONTACT & ADDRESS </div>
											</div>
											<span class="process-tab-id">2</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4  ">
										<div class="process-item widget-element-bottom process_style_3   process-tab-3">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 3 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> ACADEMIC </div>
											</div>
											<span class="process-tab-id">3</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4 column-last ">
										<div class="process-item widget-element-bottom process_style_3  process_last process-tab-4">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 4 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> OTHER  & DOCUMENTS </div>
											</div>
											<span class="process-tab-id">4</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="cleared"></div>
								</div>
								<div class="process-content content process-content-2 process_content_active">
									<h2 style="font-weight: bold;">CONTACT & ADDRESS DETAILS</h2>
									<div class="col-1">
										<div class="woocommerce-billing-fields">
										<p class="form-row form-row form-row-first validate-required validate-phone" id="phone_field">
											<input type="tel" class="input-text " name="phone" id="phone" placeholder="Mobile No.*"  value="" required pattern="[0-9]{10}"/>
										</p>
										<p class="form-row form-row form-row-
										 validate-required validate-email" id="email_field">
											<input type="email" class="input-text " name="email" id="email" placeholder="E-mail"  value="" required />
										</p>
										<p class="form-row form-row form-row-wide address-field validate-required" id="address_1_field">
											<input type="text" class="input-text " name="address_1" id="address_1" placeholder="Address Line 1*"  value=""  required pattern="[a-zA-Z\s]{15-100}" />
										</p>
										<p class="form-row form-row form-row-wide address-field" id="address_2_field">
											<input type="text" class="input-text " name="address_2" id="address_2" placeholder="Address Line 2 (optional)"  value="" pattern="[a-zA-Z\s]{15-100}" />
										</p>
										<p class="form-row form-row form-row-first validate-required validate-phone" id="phone_field">
											<input type="tel" class="input-text " name="tel" id="tel" placeholder="Telephone No."  value="" required required  pattern="[0-9]{10}"/>
										</p>
										<p class="form-row form-row form-row-last address-field validate-required" id="city_field">
											<input type="text" class="input-text " name="city" id="city" placeholder="Village / Town / City"  value=""  required pattern="[a-zA-Z\s]{3-20}" />
										</p>
										<p class="form-row form-row form-row-first address-field validate-required" id="city_field">
											<input type="text" class="input-text " name="taluka" id="taluka" placeholder="Taluka"  value=""  required pattern="[a-zA-Z\s]{3-20}" />
										</p>
										<p class="form-row form-row form-row-last address-field validate-required" id="city_field">
											<input type="text" class="input-text " name="district" id="district" placeholder="District"  value="" required pattern="[a-zA-Z\s]{3-20}"  />
										</p>
										
										<p class="form-row form-row form-row-first address-field validate-required" id="city_field">
											<input type="text" class="input-text " name="state" id="state" placeholder="State"  value="" required pattern="[a-zA-Z\s]{3-20}"  />
										</p>
										<p class="form-row form-row form-row-last address-field validate-postcode" id="postcode_field">
											<input type="text" class="input-text " name="postcode" id="postcode" placeholder="Postal / ZIP Code"  value=""  required pattern="[0-9]{6}" />
										</p>
										<div class="clear"></div>
										<div class="clear"></div>
											
												
												<p class="form-row">
													<a type="button" class="next" name="next" href="#acad" >Next</a>
												</p>
												
										</div>
									</div>

								</div>
									
								</div>
								<div class="cleared"></div>

								<div class="process process_container_3" id="acad">
								<div class="process_row" style="pointer-events: none;">
									<div class="process_cell percent_column4  ">
										<div class="process-item widget-element-bottom process_style_3   process-tab-1">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 1 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> PERSONAL </div>
											</div>
											<span class="process-tab-id">1</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4 " >
										<div class="process-item widget-element-bottom process_style_3  process-tab-2">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 2 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> CONTACT & ADDRESS </div>
											</div>
											<span class="process-tab-id">2</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4  process_active">
										<div class="process-item widget-element-bottom process_style_3 process_active  process-tab-3">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 3 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name" style="font-weight: bold;"> ACADEMIC </div>
											</div>
											<span class="process-tab-id">3</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4 column-last ">
										<div class="process-item widget-element-bottom process_style_3  process_last process-tab-4">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 4 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> OTHER  & DOCUMENTS </div>
											</div>
											<span class="process-tab-id">4</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="cleared"></div>
								</div>
								<div class="process-content content process-content-3 process_content_active">
									<h2 style="font-weight: bold;">ACADEMIC DETAILS</h2>
									<div class="col-1">
										<div class="woocommerce-billing-fields">
										<h4>Xth Details</h4>										
										<p class="form-row form-row form-row-first validate-required" id="gender_field">
											<select name="tenth" id="tenth" class="gender" style="max-height: 39px;" required>
												<option value="">Xth Board</option>
												<option value="Male" >SSC</option>
												<option value="Female" >Other</option>
											</select>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="text" class="input-text " name="tenth_year" id="tenth_year" placeholder="Xth Passing Year*"  value=""  required pattern="[0-9]{4}" />
										</p>
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
											<input type="text" class="input-text " name="tenth_percent" id="tenth_percent" placeholder="Xth Total Percentage*"  value="" required pattern="[0-1]{0-1}[0-9]{2}"/>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="text" class="input-text " name="tenth_maths_percent" id="tenth_maths_percent" placeholder="Xth Maths Percentage*"  value=""  required pattern="[0-1]{0-1}[0-9]{2}"/>
										</p>
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
											<input type="text" class="input-text " name="tenth_seat_no" id="tenth_seat_no" placeholder="Xth Seat No.*"  value=""  required pattern="[0-9]{6}" />
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="text" class="input-text " name="qualifying_exam" id="qualifying_exam" placeholder="Qualifying Exam*"  value="" required pattern="[a-zA-Z\s]{3-20}" />
										</p>
										<h4>XIIth Details</h4>
										<p class="form-row form-row form-row-first validate-required" id="gender_field">
											<select name="twelve" id="twelve" class="gender" style="max-height: 39px;" required >
												<option value="">XIIth Board</option>
												<option value="Male" >HSC</option>
												<option value="Female" >Other</option>
											</select>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="text" class="input-text " name="tw_year" id="tw_year" placeholder="XIIth Passing Year*"  value=""   required
											 pattern="[0-9]{4}"/>
										</p>
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
											<input type="text" class="input-text " name="tw_total_percent" id="tw_total_percent" placeholder="XIIth Total Percentage*"  value=""  required pattern="[0-1]{0-1}[0-9]{2}" />
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="text" class="input-text " name="tw_maths_percent" id="tw_maths_percent" placeholder="XIIth Maths Percentage*"  value=""  required pattern="[0-1]{0-1}[0-9]{2}" />
										</p>
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
											<input type="text" class="input-text " name="tw_phy_percent" id="tw_phy_percent" placeholder="XIIth Physics Percentage*"  value=""  required pattern="[0-1]{0-1}[0-9]{2}" />
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="text" class="input-text " name="tw_chem_percent" id="tw_chem_percent" placeholder="XIIth Chemistry Percentage*"  value=""  required pattern="[0-1]{0-1}[0-9]{2}" />
										</p>
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
											<input type="text" class="input-text " name="tw_eng_percent" id="tw_eng_percent" placeholder="XIIth English Percentage*"  value=""  required pattern="[0-1]{0-1}[0-9]{2}" />
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="text" class="input-text " name="tw_subj_percent" id="tw_subj_percent" placeholder="XIIth Subject Percentage*"  value=""  required pattern="[0-1]{0-1}[0-9]{2}" />
										</p>
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
											<input type="text" class="input-text " name="tw_seat_no" id="tw_seat_no" placeholder="XIIth Seat No.*"  value="" required pattern="[0-9]{6}" />
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="text" class="input-text " name="add_subject" id="add_subject" placeholder="Additional Subject for eligibility*"  value="" required pattern="[a-zA-Z\s]{3-20}"  />
										</p>
										
										<div class="clear"></div>
										<div class="clear"></div>
											
												
										</div>
										
												
									</div>
									<div class="col-2">
										<div class="woocommerce-billing-fields">
										<h4>Other Details</h4>		
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
										<input type="text" class="input-text " name="eligibility_percent" id="eligibility_percent" placeholder="Eligibility Percentage*"  value="" required pattern="[0-1]{1}[0-9]{2}" />
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
										<input type="text" class="input-text " name="cet_roll_no" id="cet_roll_no" placeholder="CET Roll No.*"  value="" required pattern="[0-9]{6}" />
										</p>										
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
										<input type="text" class="input-text " name="cet_score" id="cet_score" placeholder="CET Score*"  value="" required pattern="[0-9]{3}" />
										</p>
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
										<input type="text" class="input-text " name="jee_roll_no" id="jee_roll_no" placeholder="JEE Roll No.*"  value=""  required pattern="[0-9]{6}"/>
										</p>										
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
										<input type="text" class="input-text " name="jee_score" id="jee_score" placeholder="JEE Score*"  value=""  required pattern="[0-9]{2-3}"/>
										</p>	
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
										<input type="text" class="input-text " name="merit_no" id="merit_no" placeholder="Merit No.*"  value=""  required pattern="[0-9]{6}"/>
										</p>										
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
										<input type="text" class="input-text " name="merit_marks" id="merit_marks" placeholder="Merit Marks*"  value="" required pattern="[0-9]{2-3}" />
										</p>			
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
										<input type="text" class="input-text " name="institute_code" id="institute_code" placeholder="Institute Code*"  value="" required pattern="[0-9]{4-6}" />
										</p>										
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
										<input type="text" class="input-text " name="institute_name" id="institute_name" placeholder="Institute Name*"  value="" required pattern="[a-zA-Z\s]{5-50}"  />
										</p>	
										<p class="form-row form-row form-row-first validate-required" id="phone_field">
										<input type="text" class="input-text " name="course_name" id="course_name" placeholder="Course Name*"  value="" required pattern="[a-zA-Z\s]{3-50}"  />
										</p>
										</div>

								</div>
									
								</div>

								</div>

								<div class="cleared"></div>
												<p class="form-row">
													<a type="button" class="next" name="next" href="#docs" >Next</a>
												</p>

								<div class="process process_container_3" id="docs">
								<div class="process_row" style="pointer-events: none;">
									<div class="process_cell percent_column4">
										<div class="process-item widget-element-bottom process_style_3   process-tab-1">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 1 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> PERSONAL </div>
											</div>
											<span class="process-tab-id">1</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4">
										<div class="process-item widget-element-bottom process_style_3  process-tab-2">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 2 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> CONTACT & ADDRESS </div>
											</div>
											<span class="process-tab-id">2</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4  ">
										<div class="process-item widget-element-bottom process_style_3   process-tab-3">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 3 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name"> ACADEMIC </div>
											</div>
											<span class="process-tab-id">3</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="process_cell percent_column4 column-last process_active ">
										<div class="process-item widget-element-bottom process_style_3 process_active process_last process-tab-4">
											<div class="process-arrow"></div>
											<div class="process-top">
												<div class="process-number"> 4 </div>
											</div>
											<div class="process-bottom">
												<div class="process-name" style="font-weight: bold;"> OTHER  & DOCUMENTS </div>
											</div>
											<span class="process-tab-id">4</span>
											<div class="cleared"></div>
										</div>
									</div>
									<div class="cleared"></div>
								</div>
								<div class="process-content content process-content-4 process_content_active">
									<h2 style="font-weight: bold;">Documents</h2>
									<div class="col-1">
										
										<div class="woocommerce-billing-fields">
																				
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label for="password" style="color: #000;"> Recent Photograph <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required />	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label for="password" style="color: #000;"> DTE Verification Form <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label for="password" style="color: #000;"> SSC Marksheet <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label for="password" style="color: #000;"> HSC Marksheet <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required />	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label for="password" style="color: #000;"> Aadhar Card <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										<div class="clear"></div>

										
												
										</div>
										
										
									</div>
									<div class="col-2">

										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label for="password" style="color: #000;"> Birth Certificate <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label for="password" style="color: #000;"> College LC <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label for="password" style="color: #000;"> Domicile <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label for="password" style="color: #000;"> Income Certificate <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>		
										</p>
										<div class="clear"></div>
										<p class="form-row form-row form-row-first validate-required" style="font-size: 18px;" id="phone_field">
											<label for="password" style="color: #000;"> Caste Certificate <span class="required"></span></label>
										</p>
										<p class="form-row form-row form-row-last validate-required" id="phone_field">
											<input type="file" class="button" name="file_array[]" value="Upload" required/>	
										</p>
										
										<div class="clear"></div>
										
										</div>
										
									</div>
									<div class="process-content content process-content-4 process_content_active">
									
										<div class="col-1">
											<div class="woocommerce-billing-fields">
												<h2 style="font-weight: bold;">Other Details</h2>
												<p class="form-row form-row form-row-last validate-required" id="choicecode_field">
												<input type="text" class="input-text " name="choicecode" id="choicecode" placeholder="Choice code"  value="" required />
												</p>	
												<p class="form-row form-row form-row-first validate-required" id="seattype_field">
												<input type="text" class="input-text " name="seattype" id="seattype" placeholder="Seat type"  value=""  required/>
												</p>
																						
												<div class="clear"></div>
												<div class="clear"></div>
													<p class="form-row">
												<input type="button" onclick="document.getElementById('id01').style.display='block';displaydata();" value="Submit" class="w3-button w3-large"/>
												</p>

												
												  

												  <div id="id01" class="w3-modal">
												    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

												      <div class="w3-center"><br>
												        <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
												       
												      </div>

												      <form class="w3-container" action="/action_page.php">
												        <div class="w3-section">
												          
												        	<div id="result"></div>
												        </div>
												      </form>

												      <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
												        <button  class="w3-button w3-green">Submit</button>
												        <button onclick="document.getElementById('id01').style.display='none'" type="button" class="w3-button w3-red">Cancel</button>
												      </div>

												    </div>
												  </div>
												</div>

											</div>
										</div>
									</div>
									
								</div>
								<div class="cleared"></div>


							</div>
						</div>
						<div class="cleared"></div>
						<div class="cleared"></div>
					</div>
					<!-- end widget entry --> 
				</div>
				<div class="cleared"></div>

				
				</form>
				</div>
				</div>
				<div class="cleared"></div>
			</div>
		</div>
		
		<!-- footer start--><?php include 'footer.php' ?><!-- footer end-->
	</div>
	<!-- End Page --> 
</div>
<!-- End Site Background -->

<div id="yith-quick-view-modal">
	<div class="yith-quick-view-overlay"></div>
	<div class="yith-wcqv-wrapper">
		<div class="yith-wcqv-main">
			<div class="yith-wcqv-head"> <a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a> </div>
			<div id="yith-quick-view-content" class="woocommerce single-product"></div>
		</div>
	</div>
</div>
<script src='plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=2.5.2'></script> 
<script src='plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script> 
<script src='plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=2.5.2'></script> 
<script src='plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1'></script> 
<script src='plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=2.5.2'></script> 
<script src='plugins/yith-woocommerce-quick-view/assets/js/frontend.js?ver=1.0'></script> 
<script src='plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.5'></script> 
<script src='plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.init.min.js?ver=2.5.2'></script> 
<script src='plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=4.4.2'></script> 
<script src='plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js?ver=2.0'></script> 
<script src='js/jquery/ui/core.min.js?ver=1.11.4'></script> 
<script src='js/jquery/ui/widget.min.js?ver=1.11.4'></script> 
<script src='js/jquery/ui/accordion.min.js?ver=1.11.4'></script> 
<script src='js/script.js?ver=4.4.2'></script> 
<script src='js/jquery.easing.1.3.js?ver=4.4.2'></script> 
<script src='js/jquery.isotope.min.js?ver=4.4.2'></script> 
<script src='js/flex-slider/jquery.flexslider-min.js?ver=4.4.2'></script> 
<script src='js/magnific-popup/jquery.magnific-popup.js?ver=4.4.2'></script> 
<script src='js/jquery.tubular.1.0.js?ver=4.4.2'></script> 
<script src='js/masonry.pkgd.min.js?ver=4.4.2'></script> 
<script src='js/imagesloaded.pkgd.min.js?ver=4.4.2'></script> 
<script src='js/comment-reply.min.js?ver=4.4.2'></script> 
<script src='js/wp-embed.min.js?ver=4.4.2'></script> 
<script src='js/underscore.min.js?ver=1.6.0'></script> 
<script src='js/wp-util.min.js?ver=4.4.2'></script> 
<script src='plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=2.5.2'></script>
<script>
	/* <![CDATA[ */
	var megamenu = {"effect":{"fade":{"in":{"animate":{"opacity":"show"}},"out":{"animate":{"opacity":"hide"}}},"slide":{"in":{"animate":{"height":"show"},"css":{"display":"none"}},"out":{"animate":{"height":"hide"}}}},"fade_speed":"fast","slide_speed":"fast","timeout":"300"};
	/* ]]> */
</script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script>
		$(document).ready(function(){
		  // Add smooth scrolling to all links
		  $("a").on('click', function(event) {
		  	
		    // Make sure this.hash has a value before overriding default behavior
		    if (this.hash !== "") {
		      // Prevent default anchor click behavior
		      event.preventDefault();

		      // Store hash
		      var hash = this.hash;

		      // Using jQuery's animate() method to add smooth page scroll
		      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		   
		        // Add hash (#) to URL when done scrolling (default click behavior)
		        window.location.hash = hash;
		      });
		    } // End if
		  });
		});

		
		</script>
		<script type="text/javascript">
			function displaydata() {
				var first_name=document.getElementById("first_name").value;
				document.getElementById("result").innerHTML=first_name;
			}
												          
		</script>
</body>
</html>
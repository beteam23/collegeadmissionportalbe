<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
<link rel="icon" type="image/png" href="images/tseclogo.png">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>About Us</title>
<link rel='stylesheet' id='layerslider-css'  href='css/layerslider.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='ls-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='plugins/revslider/public/assets/css/settings.css?ver=5.0.9' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css'  href='plugins/woocommerce/assets/css/woocommerce-layout.css?ver=2.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=2.5.2' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='plugins/woocommerce/assets/css/woocommerce.css?ver=2.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='megamenu-css'  href='maxmegamenu/style.css?ver=762094' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-quick-view-css'  href='plugins/yith-woocommerce-quick-view/assets/css/yith-quick-view.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='plugins/woocommerce/assets/css/prettyPhoto.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css'  href='plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-main-css'  href='plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-font-awesome-css'  href='plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimp-for-wp-checkbox-css'  href='plugins/mailchimp-for-wp/assets/css/checkbox.min.css?ver=2.3.7' type='text/css' media='all' />
<link rel='stylesheet' id='wope-gfont-Montserrat-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-gfont-Open-Sans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-default-style-css'  href='style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-responsive-css'  href='responsive.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='flexslider-style-css'  href='js/flex-slider/flexslider.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-style-css'  href='js/magnific-popup/magnific-popup.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-font-awesome-css'  href='font-awesome/css/font-awesome.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wope-font-pe-icon-7-stroke-css'  href='pe-icon-7-stroke/css/pe-icon-7-stroke.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimp-for-wp-form-css'  href='plugins/mailchimp-for-wp/assets/css/form.min.css?ver=2.3.7' type='text/css' media='all' />
<script src='plugins/LayerSlider/static/js/greensock.js?ver=1.11.8'></script>
<script src='js/jquery/jquery.js?ver=3.0.0'></script>
<script src='js/jquery/jquery-migrate.min.js?ver=3.0.1'></script>
<script src='plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js?ver=5.3.2'></script>
<script src='plugins/LayerSlider/static/js/layerslider.transitions.js?ver=5.3.2'></script>
<script src='plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.0.9'></script>
<script src='plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.0.9'></script>
<script src='js/hoverIntent.min.js?ver=1.8.1'></script>
<script src='plugins/megamenu/js/maxmegamenu.js?ver=1.8.2'></script>
<link rel='stylesheet' href='css/custom.css' type='text/css' media='all' />
</head>
<body class="page page-template-default">
<div id="background" style="">
	<div id="page">
		<div id="header" class="header-text-white header-transparent heading-align-center " >
			<div class='header_bg'></div>
			<div class="header_content">
				<div id="top-bar">
					<div class="wrap">
						<div class="left-top-bar">
							<div class="contact-detail-line">
								<ul>
									<li><i class="fa fa-phone"></i> +91–022–2649 5808 </li>
									<li><i class="fa fa-envelope-o"></i> admission@tsec.edu </li>
									<li><i class="fa fa-clock-o"></i> 10AM - 5PM </li>
								</ul>
							</div>
						</div>
						<div class="right-top-bar">
							<div class="header-social"> <a target="_blank" href=" feed/ "><i class="fa fa-rss"></i></a> <a target="_blank"  href="#"><i class="fa fa-behance"></i></a> <a target="_blank"  href="#"><i class="fa fa-dribbble"></i></a> <a target="_blank"  href="#"><i class="fa fa-facebook"></i></a>
								<div class="cleared"></div>
							</div>
							
						</div>
						<div class="cleared"></div>
					</div>
				</div>
				<div id="top-bar-open">
					<div class="wrap">
						<div id="topbar-open"><i class="fa fa-angle-down"></i></div>
					</div>
				</div>
				<div class="wrap">
					<div class="left-header">
						<div class="site-logo">
							<h1> <a class="logo-image" href=""> <img class="logo-normal" alt="Megaw" src="images/tseclogo.png" /> <img class="logo-retina"   alt="Megaw" src="images/tseclogo.png" /> </a> </h1>
						</div>
					</div>
		
					<div class="right-header">
						<div class="main-menu">
							<div id="mega-menu-wrap-main-menu" class="mega-menu-wrap">
								<div class="mega-menu-toggle"></div>
								<ul id="mega-menu-main-menu" class="mega-menu mega-menu-horizontal mega-no-js" data-event="hover" data-effect="" data-second-click="close" data-breakpoint="600">
									<li class='mega-menu-item mega-menu-item-type-custom '><a href="index.php">HOME</a></li>
								</ul>
							</div>
						</div>
						<!-- End Main Menu --> 
					</div>
					<div class="cleared"></div>
					<div id="toggle-menu-button"><i class="fa fa-align-justify"></i></div>
					<div class="toggle-menu">
						<div class="menu-toggle-menu-container">
							<ul id="menu-toggle-menu" class="menu">
								<li class='mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom  mega-menu-item-has-children mega-menu-item-18 mega-align-bottom-left mega-menu-flyout'><a href="index.php">HOME</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
					
				<!-- End Header Wrap -->  
				<!-- End Header --> 
				
				<!--Header Content for this page-->
				
				
			</div>
			<!-- End Header Content --> 
		</div>
		<!-- end Header -->
		<div class="cleared"></div>
		
	
		<!-- BEGIN CONTENT -->
		<div id="body">
			<div id="content-section1" class="content-section black-text white-bg top-spacing-medium bottom-spacing-medium title-spacing-medium align-center " style="">
				<div class="wrap-column">
					<div class="content-column3_3">
						<div class="widget-entry">
							<div class="column1">
								<div class="section-widget-heading">
									<h3 class="section-widget-title">ABOUT TSEC</h3>
								</div>
								<div class="content content-box-content">
									<p style="text-align: justify;color: #000;">Established in 1983, Thadomal Shahani Engineering College is founded by the Hyderabad (Sind) National Collegiate Board. Thadomal Shahani Engineering College (TSEC) is recognized by the Government of Maharashtra. This college is approved by the All India Council for Technical Education (AICTE), Affiliated to the University of Mumbai, TSEC is graded as an ‘A’ grade educational institution. It offers a 4-year Bachelor of Engineering (B.E.) Degree program in Information Technology, Computer Engineering, Electronics & Telecommunication , Biomedical Engineering, Biotechnology and Chemical Engineering. The College offers Ph.D. program in the subject of Information Technology, Computer Engineering, Electronics & Telecommunication. 5 Ph.D. scholars were awarded with Ph.D. degree & at present we are enrolled with 35 research Scholars. We offer research mainly in the following topics: data analytics, artificial intelligence, cloud computing, Internet of Things (IOT), data mining, Ecommerce & E governance. Various branches got accreditation starting from 2005. Of these branches Electronics & Telecommunication , Biomedical Engineering, Information Technology branches are accredited at the degree level by the National Board of Accreditation (NBA) of the AICTE.  We have applied for Re-Accreditation of 5 existing branches. We satisfy cadre ratio & we are having more than sufficient full-time faculties who are manning under graduate programme & the average experience is around 15 years. We are hugely succeeding in creating an academic ambience where learners are increasingly succeeding to unleash their potential.  We are massively successful in placing pupils/ graduates whoever is willing to join enterprises. Our graduates are also getting enrolled in institutes of higher learning for their postgraduate programmes of the choicest branch & institute both in India & Abroad. </p>
									
								</div>
							</div>
							<div class="cleared"></div>
							<div class="cleared"></div>
						</div>
						<!-- end widget entry --> 
					</div>
					
				</div>
			</div>
			<div id="content-section1" class="content-section black-text white-bg top-spacing-medium bottom-spacing-medium title-spacing-medium align-center " style="">
				<div class="wrap-column">
					<div class="content-column2_1">
						<div class="widget-entry">
							<div class="column1">
								<div class="section-widget-heading">
									<h3 class="section-widget-title">MISSON</h3>
								</div>
								<div class="content content-box-content" >
									<ul style="list-style-type:disc;text-align: justify; color: #000;">
										<h4>Focusing and practicing:</h4>
										<li>Product and processes innovation</li>
										<li>Leveraging human cognitive and behavioral science for creating instructional content</li>
										<li>Pervasive and ubiquitous Information Communication Technologies for customized content for learning</li>
										<li>Acknowledge and facilitate various learning styles and learning abilities</li>
										<li>Migrating from teaching paradigm to learning paradigm</li>
										<li>Every day discourse shall inculcate research culture and further the cause of societal advancement</li>
										<li>Understand various markets and cultures</li>
										<li>Collaborative learning and emotional integrity</li>
										<li>Sensitizing about opportunities in Energy, Education, Environment and Health care sectors</li>
										<li>Extensively promoting computer aided design,analysis and manufacturing procedures</li>
										<li>Theoretical rigor to develop conceptual clarity</li>
										<li>Modeling and design of experiments to inculcate culture of investigation</li>
										<li>Helping foot print on Project management and collaborative human endeavor</li>
										<li>Interdisciplinary studies and exposure to functional areas</li>
									</ul>
								</div>
							</div>
							<div class="cleared"></div>
							<div class="cleared"></div>
						</div>
						<!-- end widget entry -->
					 </div>
					<div class="content-column2_1">
						<div class="widget-entry">
							<div class="column1">
								<div class="section-widget-heading">
									<h3 class="section-widget-title">Vision</h3>
								</div>
								<div class="content content-box-content">
									<ul style="list-style-type:disc;text-align: justify; color: #000;">
										<h4>Perpetuating and transcending the processes of:</h4>
										<li>Contributing to evolving supply chain of human capital for National Economy</li>
										<li>Creating entrepreneurs and ‘game changers’ to support heightened level of economic activities underpinning ever increasing human aspiration</li>
										<li>Helping the Nation evolve as a total solution provider</li>
										<li>Value and wealth creation for the mankind</li>
									</ul>
								</div>
							</div>
							<div class="cleared"></div>
							<div class="cleared"></div>
						</div>
						<!-- end widget entry --> 
					</div>
					<div class="cleared"></div>
				</div>
				<!-- end wrap-column no-wrap --> 
			</div>
			<!-- end content section -->
			
		
		<!-- footer start--><?php include 'footer.php' ?><!-- footer end-->
	</div>
	<!-- End Page --> 
</div>
<!-- End Site Background -->

<div id="yith-quick-view-modal">
	<div class="yith-quick-view-overlay"></div>
	<div class="yith-wcqv-wrapper">
		<div class="yith-wcqv-main">
			<div class="yith-wcqv-head"> <a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a> </div>
			<div id="yith-quick-view-content" class="woocommerce single-product"></div>
		</div>
	</div>
</div>
<script src='plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=2.5.2'></script> 
<script src='plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script> 
<script src='plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=2.5.2'></script> 
<script src='plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1'></script> 
<script src='plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=2.5.2'></script> 
<script src='plugins/yith-woocommerce-quick-view/assets/js/frontend.js?ver=1.0'></script> 
<script src='plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.5'></script> 
<script src='plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.init.min.js?ver=2.5.2'></script> 
<script src='plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=4.4.2'></script> 
<script src='plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js?ver=2.0'></script> 
<script src='js/jquery/ui/core.min.js?ver=1.11.4'></script> 
<script src='js/jquery/ui/widget.min.js?ver=1.11.4'></script> 
<script src='js/jquery/ui/accordion.min.js?ver=1.11.4'></script> 
<script src='js/script.js?ver=4.4.2'></script> 
<script src='js/jquery.easing.1.3.js?ver=4.4.2'></script> 
<script src='js/jquery.isotope.min.js?ver=4.4.2'></script> 
<script src='js/flex-slider/jquery.flexslider-min.js?ver=4.4.2'></script> 
<script src='js/magnific-popup/jquery.magnific-popup.js?ver=4.4.2'></script> 
<script src='js/jquery.tubular.1.0.js?ver=4.4.2'></script> 
<script src='js/masonry.pkgd.min.js?ver=4.4.2'></script> 
<script src='js/imagesloaded.pkgd.min.js?ver=4.4.2'></script> 
<script src='js/comment-reply.min.js?ver=4.4.2'></script> 
<script src='js/wp-embed.min.js?ver=4.4.2'></script> 
<script src='js/underscore.min.js?ver=1.6.0'></script> 
<script src='js/wp-util.min.js?ver=4.4.2'></script> 
<script src='plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=2.5.2'></script>
<script>
	/* <![CDATA[ */
	var megamenu = {"effect":{"fade":{"in":{"animate":{"opacity":"show"}},"out":{"animate":{"opacity":"hide"}}},"slide":{"in":{"animate":{"height":"show"},"css":{"display":"none"}},"out":{"animate":{"height":"hide"}}}},"fade_speed":"fast","slide_speed":"fast","timeout":"300"};
	/* ]]> */
</script>
</body>
</html>
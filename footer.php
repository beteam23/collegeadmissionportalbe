<div id="footer">
			<div class="wrap-column">
				<div id="footer-widget-container" >
					<div class="footer-column">
						<div id="text-2" class="footer-widget widget_text content" style="color: #fff;">
							<div class="footer-widget-title">ABOUT US</div>
							<div class="textwidget">Thadomal Shahani Engineering College (TSEC) is an engineering and research institute in Mumbai, India. 
							It is the first and the oldest private engineering institute affiliated to the University of Mumbai.
								<div class="footer-social"> 
								<!--<a target="_blank" href="#"><i class="fa fa-dribbble"></i></a>--> 
								<a target="_blank" href="https://www.facebook.com/tsec.edu/"><i class="fa fa-facebook"></i></a> 
								<a target="_blank" href="https://www.instagram.com/tsec_official/"><i class="fa fa-instagram"></i></a> 
								<a target="_blank" href="https://in.linkedin.com/school/thadomal-shahani-engineering-college/"><i class="fa fa-linkedin"></i></a> 
								<!--<a target="_blank" href="#"><i class="fa fa-twitter"></i></a> --></div>
							</div>
						</div>
					</div>
					<div class="footer-column">
						<div id="wope_multi_line_menu_widget-2" style="margin-left:35%;" class="footer-widget widget_wope_multi_line_menu_widget content" >
							<div class="footer-widget-title">Quick Links</div>
							<div class='multi_line_menu_container_2'>
								<div class="menu-footer-widget-menu-1-container" >
									<ul id="menu-footer-widget-menu-1" class="menu"	>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3"><a href="index.php">Home</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4"><a href="">About</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5"><a href="contactus.php">Contact Us</a></li>
										<!--<li  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6"><a href="#">Clients</a></li>-->
									</ul>
								</div>
								<!--<div class="menu-footer-widget-menu-2-container">
									<ul id="menu-footer-widget-menu-2" class="menu">
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7"><a href="#">Support Center</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8"><a href="#">Megaw Forum</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9"><a href="#">Ticket System</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10"><a href="#">Contact Us</a></li>
									</ul>
								</div>
								-->
								<div class="cleared"></div>
							</div>
						</div>
					</div>
					<div class="footer-column">
						<div id="text-3" class="footer-widget widget_text content" style="color: #fff;">
							<div class="footer-widget-title">Contact Us</div>
							<div class="textwidget"><b>Office</b> : P. G. Kher Marg, (32nd Road), TPS-III,<br>
								Off Linking Road, Bandra (W),<br>
								Mumbai - 400050.<br>
								<b>Phone</b> : +91–022–2649 5808<br>
								<b>E-Mail</b> : gtthampi@gmail.com<br>
								<b>Web</b> : https://tsec.edu/</div>
						</div>
					</div>
					<div class="cleared"></div>

				</div>

				<!-- End Footer Widget Container--> 
			</div>
		</div>
		<!-- End Footer -->
		<div id="footer-bottom" >
			<div class="wrap">
				<div class="widget-entry" style="padding: 0 5%;">
					<h3 style="margin-bottom: 20px;color: #fff;"> Developed By </h1>
							<div class="widget-column1-4 ">
								<div class="team-item widget-element-bottom team-style-2">
									<div class="team-detail">
										<div class="team-heading">
											<div class="team-name" style="color: #fff;"> Laveena Valecha </div>
<!-- 											<div class="team-sub-name"> CEO &amp; FOUNDER </div> -->
										</div>
									</div>
								</div>
							</div>
							<div class="widget-column1-4 ">
								<div class="team-item widget-element-bottom team-style-2">
									<div class="team-detail">
										<div class="team-heading">
											<div class="team-name" style="color: #fff;"> Kiran Khatri </div>
										</div>
									</div>
								</div>
							</div>
							<div class="widget-column1-4 ">
								<div class="team-item widget-element-bottom team-style-2">
									<div class="team-detail">
										<div class="team-heading">
											<div class="team-name" style="color: #fff;"> Jayesh Lilani </div>
										</div>
									</div>
								</div>
							</div>
							<div class="widget-column1-4 column-last">
								<div class="team-item widget-element-bottom team-style-2">
									<div class="team-detail">
										<div class="team-heading">
											<div class="team-name" style="color: #fff;"> Piyush Khanna </div>
										</div>
									</div>
								</div>
							</div>
							<div class="cleared"></div>
							<div class="cleared"></div>
						</div>
						<!-- end widget entry --> </div>
					<div class="cleared"></div>
				<div class="footer-copyright"> Copyright © 2019 Thadomal Shahani Engineering College. (TSEC). All Rights Reserved </div>
			</div>
		</div>